# CIFS - Montando pastas do Windows e Samba no Linux.

## Debian e derivados: 
```shell
apt update
apt install cifs-utils
```
## RedHat e derivados:
```shell
yum update
yum install cifs-utils
```

## SMB:
### Criar um arquivo de credeicial no usuário que irá montar o arquivo do Windows e aplicar a permissão "400":

```shell
cat > /root/.smbcred << EOF
username=vagrant
password=vagrant
#domain=domain
EOF
chmod 400 /root/.smbcred
```
### Criar o diretórios onde serão montada pasta remota:

```shell
mkdir -p /mnt/winmount
```
### Habilitar ponto de montagem manualmente:

```shell
mount.cifs -v //192.168.1.200/winmount/ /mnt/winmount --verbose -o credentials=/root/.smbcred
mount.cifs -v //192.168.1.24/winserver/ /mnt/winmount --verbose -o credentials=/root/.smbcred
```

### Habilitar ponto de montagem via fstab:

```shell
//192.168.1.24/winserver/ /mnt/winmount cifs credentials=/root/.smbcred 0 0
```